//ТЗ 1
const product = {
    name: "Парадичка",
    price: 100,
    discount: 30,
    calculateDiscountedPrice: function () {
        const discountedPrice = this.price - (this.price * this.discount) / 100;
        console.log(`Повна ціна з урахуванням знижки: ${discountedPrice}`);
        return discountedPrice;
    },
};
product.calculateDiscountedPrice();

//ТЗ 2
function createGreeting(obj) {
    const { name, age } = obj;
    const greeting = `Привіт, мені ${age} років`;
    return greeting;
}
const userInput = {
    name: prompt("Введіть ваше ім'я:"),
    age: prompt("Введіть ваш вік:"),
};
const greetingMessage = createGreeting(userInput);
alert(greetingMessage);

//ТЗ 3
function deepClone(obj) {
    if (obj === null || typeof obj !== "object") {
        return obj;
    }
    if (Array.isArray(obj)) {
        // Клонуємо масив
        const clonedArray = [];
        for (let i = 0; i < obj.length; i++) {
            clonedArray[i] = deepClone(obj[i]);
        }
        return clonedArray;
    }
    const clonedObject = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            clonedObject[key] = deepClone(obj[key]);
        }
    }
    return clonedObject;
}
const originalObject = {
    name: "John",
    age: 25,
    details: {
        city: "New York",
        hobbies: ["reading", "traveling"],
    },
};
const clonedObject = deepClone(originalObject);

console.log("Оригінальний об'єкт:", originalObject);
console.log("Клонований об'єкт:", clonedObject);